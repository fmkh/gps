namespace GPX {
    GPS::GridWorldRoute singlePositionLog(const std::string &);
    GPS::GridWorldRoute generateLogFile(const std::string &);
}